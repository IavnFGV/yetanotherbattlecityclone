package es.esy.iavnfgv.fx.sprite;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.VitalState;
import es.esy.iavnfgv.fx.app.yabcc.event.IVitalStateChangeEvent;
import es.esy.iavnfgv.fx.app.yabcc.playground.Playground;
import es.esy.iavnfgv.fx.app.yabcc.unit.UnitTest;
import eu.lestard.easydi.EasyDI;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Created by GFH on 15.01.2016.
 */
public class SpriteVisualTest extends Application {
    public Pane pane;
    protected EasyDI easyDI = new EasyDI();

    {
        easyDI.bindInterface(Playground.class, UnitTest.UnitTestPlayground.class);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        pane = new Pane();
        Scene scene = new Scene(pane, 800, 600);
        primaryStage.setScene(scene);
        //WaterTile waterTile = easyDI.getInstance(WaterTile.class);
        WaterFxSprite waterFxSprite = easyDI.getInstance(WaterFxSprite.class);
        waterFxSprite.unit.handle(new IVitalStateChangeEvent() {
            @Override
            public VitalState getNewVitalState() {
                return VitalState.ACTIVE;
            }
        });
        pane.getChildren().add(waterFxSprite);

        primaryStage.show();
    }


}
