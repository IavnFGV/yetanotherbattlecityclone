package es.esy.iavnfgv.fx.sprite;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.PlaygroundState;
import es.esy.iavnfgv.fx.app.yabcc.enumeration.VitalState;
import es.esy.iavnfgv.fx.app.yabcc.event.IVitalStateChangeEvent;
import es.esy.iavnfgv.fx.app.yabcc.playground.Playground;
import es.esy.iavnfgv.fx.app.yabcc.unit.Unit;
import eu.lestard.easydi.EasyDI;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.stage.Stage;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testfx.framework.junit.ApplicationTest;

import javax.inject.Singleton;
import java.util.List;

/**
 * Created by GFH on 21.01.2016.
 */
@Ignore
public class SpriteTest extends ApplicationTest {
    protected static EasyDI easyDI;
    private static Logger log = LoggerFactory.getLogger(SpriteTest.class);
    FxSprite testSprite;
    Unit testUnit;

    @BeforeClass
    public static void classSetUp() throws Exception {
        log.info("Creating EasyDI instance");
        easyDI = new EasyDI();
        easyDI.bindInterface(Playground.class, SpriteTestPlayground.class);
        log.info("EasyDI is created");
    }

    @Override
    public void start(Stage stage) throws Exception {

    }

    @Before
    public void setUp() throws Exception {
        initTestUnit(getTestClass());
        log.info("Test instance of class: " + testUnit.getClass());
        log.info("Test sprite instance of class: " + testSprite.getClass());

    }

    protected void initTestUnit(Class<? extends FxSprite> aClass) {
        testSprite = easyDI.getInstance(aClass);
        testUnit = testSprite.unit;
    }

    protected Class<? extends FxSprite> getTestClass() {
        return FxSprite.class;
    }

    @Test
    public void testBasicBehaviour() {
        log.info("Send Activation message");
        testUnit.handle(new IVitalStateChangeEvent() {
            @Override
            public VitalState getNewVitalState() {
                return VitalState.ACTIVE;
            }
        });
        log.info("Send Sleep message");
        testUnit.handle(new IVitalStateChangeEvent() {
            @Override
            public VitalState getNewVitalState() {
                return VitalState.SLEEP;
            }
        });
        log.info("full sprite must be invisible");
        TestCase.assertTrue(!testSprite.isVisible());
        log.info("OK");
    }


    @Singleton
    public static class SpriteTestPlayground implements Playground {
        @Override
        public ReadOnlyObjectProperty<PlaygroundState> stateProperty() {
            return null;
        }

        @Override
        public double getTileWidth() {
            return 8;
        }

        @Override
        public double getTileHeight() {
            return 8;
        }

        @Override
        public ObservableList<Unit> getUnitList() {
            return null;
        }

        @Override
        public boolean isInWorldBounds(List<Bounds> boundsList) {
            return false;
        }

        @Override
        public boolean load(int x, int y, Unit unit) {
            return false;
        }
    }
}
