package es.esy.iavnfgv.fx.sprite;

import junit.framework.TestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by GFH on 21.01.2016.
 */
public class WaterSpriteTest extends SpriteTest {
    private static Logger log = LoggerFactory.getLogger(WaterSpriteTest.class);

    @Override
    protected Class<? extends FxSprite> getTestClass() {
        return WaterFxSprite.class;
    }

    @Override
    public void testBasicBehaviour() {
        super.testBasicBehaviour();
        log.info("water image view must be  invisible");
        TestCase.assertTrue(!(
                ((FxSprite.SpriteAnimation) testSprite.backAnimations.get(YabccSprite.TILE_WATER)).imageView.isVisible()));
        log.info("OK");
    }
}
