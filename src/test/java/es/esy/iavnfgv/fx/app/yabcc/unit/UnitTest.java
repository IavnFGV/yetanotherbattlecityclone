package es.esy.iavnfgv.fx.app.yabcc.unit;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.PlaygroundState;
import es.esy.iavnfgv.fx.app.yabcc.event.*;
import es.esy.iavnfgv.fx.app.yabcc.playground.Playground;
import eu.lestard.easydi.EasyDI;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.stage.Stage;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;

import static java.util.Arrays.asList;
import static junit.framework.TestCase.assertTrue;

/**
 * Created by GFH on 11.01.2016.
 */
@Ignore
public class UnitTest /*extends ApplicationTest*/ {

    protected static EasyDI easyDI;
    protected static List<Class<? extends IChangeEvent>> basicChangeEvents =
            asList(IVitalStateChangeEvent.class
                    , IPauseStateChangeEvent.class
                    , IBoundsChangeEvent.class);
    private static Logger log = LoggerFactory.getLogger(UnitTest.class);
    protected Unit testUnit;


    @BeforeClass
    public static void classSetUp() throws Exception {
        log.info("Creating EasyDI instance");
        easyDI = new EasyDI();
        easyDI.bindInterface(Playground.class, UnitTestPlayground.class);
        log.info("EasyDI is created");
    }

    @Before
    public void setUp() throws Exception {
        testUnit = createTestUnit(getTestClass());
        log.info("Test instance of class: " + testUnit.getClass());
        log.info(Thread.currentThread().getName());
    }

    protected <U extends Unit> U createTestUnit(Class<U> uClass) {
        return easyDI.getInstance(uClass);
    }

    protected Class<? extends Unit> getTestClass() {
        return Unit.class;
    }

    @Test
    public void testEasyDI() throws Exception {
        log.info("EasyDI TEST START");
        assertTrue(testUnit.playground != null);
        log.info("Playground INJECTED");
        log.info("EasyDI TEST SUCCESS");
    }

    @Test
    public void testEventsHandlers() {
        log.info("testEventsHandlers START");
        List<Class<? extends IChangeEvent>> classes = getChangeEventList();
        classes.forEach(aClass -> {
            TestCase.assertTrue("[" + aClass + "] handler is absent in EventHandlers list"
                    , isHandlerPresent(testUnit.eventHandlers, aClass));
        });
        log.info("all handlers exist N = [" + classes.size() + "] :" + classes);
    }

    protected List<Class<? extends IChangeEvent>> getChangeEventList() {
        return basicChangeEvents;
    }

    protected static boolean isHandlerPresent(List<EventHandler> handlers, Class<? extends IChangeEvent> aClass) {
        if (handlers == null)
            throw new NullPointerException("Handlers list is null");
        if (handlers.size() == 0)
            throw new RuntimeException("Handlers list is empty");
        return handlers.stream().anyMatch((eventHandler -> eventHandler.getEventObjectType() == aClass));
    }

    //    @Override
    public void start(Stage stage) throws Exception {

    }


    @Singleton
    public static class UnitTestPlayground implements Playground {
        @Inject
        public UnitTestPlayground() {
        }

        @Override
        public ReadOnlyObjectProperty<PlaygroundState> stateProperty() {
            return null;
        }

        @Override
        public double getTileWidth() {
            return 8;
        }

        @Override
        public double getTileHeight() {
            return 8;
        }

        @Override
        public ObservableList<Unit> getUnitList() {
            return null;
        }

        @Override
        public boolean isInWorldBounds(List<Bounds> boundsList) {
            return false;
        }

        @Override
        public boolean load(int x, int y, Unit unit) {
            return false;
        }
    }
}
