package es.esy.iavnfgv.fx.app.yabcc.unit.moveable;

import es.esy.iavnfgv.fx.app.yabcc.event.*;
import es.esy.iavnfgv.fx.app.yabcc.unit.UnitTest;
import org.junit.Ignore;

import java.util.LinkedList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Created by GFH on 12.01.2016.
 */
@Ignore("Its abstract now. Nothing to test")
public class TestMoveable extends UnitTest {
    protected static List<Class<? extends IChangeEvent>> moveableEvents =
            asList(ISpeedChangeEvent.class
                    , IDirectionChangeEvent.class
                    , IEngineStateChangeEvent.class
                    , IMoveEvent.class);

    @Override
    protected List<Class<? extends IChangeEvent>> getChangeEventList() {
        List<Class<? extends IChangeEvent>> all = new LinkedList<>(basicChangeEvents);
        all.addAll(moveableEvents);
        return all;
    }
}
