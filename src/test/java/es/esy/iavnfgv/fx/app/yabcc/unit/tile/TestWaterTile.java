package es.esy.iavnfgv.fx.app.yabcc.unit.tile;

import es.esy.iavnfgv.fx.app.yabcc.unit.Unit;
import es.esy.iavnfgv.fx.app.yabcc.unit.UnitTest;

/**
 * Created by GFH on 12.01.2016.
 */
public class TestWaterTile extends UnitTest {
    @Override
    protected Class<? extends Unit> getTestClass() {
        return WaterTile.class;
    }
}
