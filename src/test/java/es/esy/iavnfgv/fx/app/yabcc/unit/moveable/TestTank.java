package es.esy.iavnfgv.fx.app.yabcc.unit.moveable;

import es.esy.iavnfgv.fx.app.yabcc.unit.Unit;

/**
 * Created by GFH on 12.01.2016.
 */
public class TestTank extends TestMoveable {
    @Override
    protected Class<? extends Unit> getTestClass() {
        return Tank.class;
    }
}
