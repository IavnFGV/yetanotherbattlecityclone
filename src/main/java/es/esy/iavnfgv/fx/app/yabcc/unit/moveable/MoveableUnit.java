package es.esy.iavnfgv.fx.app.yabcc.unit.moveable;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.Direction;
import es.esy.iavnfgv.fx.app.yabcc.enumeration.EngineState;
import es.esy.iavnfgv.fx.app.yabcc.enumeration.MoveOnWorldBounds;
import es.esy.iavnfgv.fx.app.yabcc.event.*;
import es.esy.iavnfgv.fx.app.yabcc.playground.Playground;
import es.esy.iavnfgv.fx.app.yabcc.unit.Unit;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * Created by GFH on 11.01.2016.
 */
public abstract class MoveableUnit extends Unit {
    static Map<Direction, BiFunction<Point2D, Integer, Point2D>> newPositionMap = new
            HashMap<>();
    private static Logger log = LoggerFactory.getLogger(MoveableUnit.class);

    static {
        newPositionMap.put(Direction.UP, ((point2D, delta) -> new Point2D(point2D.getX(), point2D.getY() - delta)));
        newPositionMap.put(Direction.RIGHT, ((point2D, delta) -> new Point2D(point2D.getX() + delta, point2D.getY())));
        newPositionMap.put(Direction.DOWN, ((point2D, delta) -> new Point2D(point2D.getX(), point2D.getY() + delta)));
        newPositionMap.put(Direction.LEFT, ((point2D, delta) -> new Point2D(point2D.getX() - delta, point2D.getY())));
    }

    protected ObjectProperty<Direction> direction;
    protected IntegerProperty speed;
    protected ObjectProperty<EngineState> engineState;


    public MoveableUnit(Playground playground) {
        super(playground);
        eventHandlers.add(createSpeedChangeEventHandler());
        eventHandlers.add(createDirectionChangeEventHandler());
        eventHandlers.add(createEngineChangeEventHandler());
        eventHandlers.add(createMoveEventHandler());
    }

    protected EventHandler<ISpeedChangeEvent> createSpeedChangeEventHandler() {
        return new SpeedChangeEventHandler();
    }

    protected EventHandler<IDirectionChangeEvent> createDirectionChangeEventHandler() {
        return new BaseDirectionChangeEventHandler();
    }

    protected EventHandler<IEngineStateChangeEvent> createEngineChangeEventHandler() {
        return new EngineStateChangeEventHandler();
    }

    protected EventHandler<IMoveEvent> createMoveEventHandler() {
        return new BaseMoveEventHandler();
    }

    public Direction getDirection() {
        return direction.get();
    }

    public void setDirection(Direction direction) {
        this.direction.set(direction);
    }

    public ObjectProperty<Direction> directionProperty() {
        return direction;
    }

    public int getSpeed() {
        return speed.get();
    }

    public void setSpeed(int speed) {
        this.speed.set(speed);
    }

    public IntegerProperty speedProperty() {
        return speed;
    }

    public EngineState getEngineState() {
        return engineState.get();
    }

    public void setEngineState(EngineState engineState) {
        this.engineState.set(engineState);
    }

    public ObjectProperty<EngineState> engineStateProperty() {
        return engineState;
    }

    protected boolean canMove() {
        return true;
    }

    protected abstract MoveOnWorldBounds isMoveAllowed(boolean isInWorldBounds);

    protected class SpeedChangeEventHandler implements EventHandler<ISpeedChangeEvent> {
        @Override
        public void handle(ISpeedChangeEvent event) {
            log.debug("SpeedChangeEventHandler.handle with parameters " + "event = [" + event + "]");
            synchronized (MoveableUnit.this) {
                if (MoveableUnit.this.getSpeed() != event.getNewSpeed()) {
                    log.debug("SpeedChangeEventHandler.handle  Changing value from [" + MoveableUnit.this.getSpeed() +
                            "] to [" + event.getNewSpeed() + "]");
                    MoveableUnit.this.setSpeed(event.getNewSpeed());
                }
            }
        }

        @Override
        public Class<ISpeedChangeEvent> getEventObjectType() {
            return ISpeedChangeEvent.class;
        }
    }

    protected class BaseMoveEventHandler implements EventHandler<IMoveEvent> {

        protected MoveableUnit moveable = MoveableUnit.this;

        @Override
        public void handle(IMoveEvent event) {
            if (!moveable.canMove()) return;
            Integer deltaPosition = (moveable.getSpeed());
            if (deltaPosition == null) return;
            Direction direction = moveable.getDirection();
            if (direction == null) return;
            EngineState engineState = moveable.getEngineState();
            if (engineState == null) return;
            if (engineState == EngineState.ENABLED) {
                List<Bounds> curBounds = moveable.getBoundsList();
                List<Bounds> newBounds = new LinkedList<>();
                for (Bounds bounds : curBounds) {
                    Point2D curPosition = new Point2D(bounds.getMinX(), bounds.getMinY());
                    Point2D newPosition = newPositionMap.get(direction).apply(curPosition, deltaPosition);
                    newBounds.add(
                            new BoundingBox(
                                    newPosition.getX(),
                                    newPosition.getY(),
                                    bounds.getWidth(),
                                    bounds.getHeight()));
                }
                confirmNewPosition(newBounds);
            }
        }

        protected void confirmNewPosition(List<Bounds> newBounds) {
            boolean isInWorldBounds = playground.isInWorldBounds(newBounds);
            switch (moveable.isMoveAllowed(isInWorldBounds)) {
                case ALLOW:
                    allowChange(newBounds);
                    break;
                case STOP:
                    break;
                case DESTROY:
                    destroyChange(newBounds);
                    break;
            }
        }

        protected void allowChange(List<Bounds> newBounds) {
            MoveableUnit.this.handle((IBoundsChangeEvent) () -> newBounds);
            //  moveable.getBoundsList().clear();
            //  moveable.getBoundsList().addAll(newBounds);
        }

        protected void destroyChange(List<Bounds> newBounds) {
            //TODO SEND APPROPRIATE EVENT????
        }

        @Override
        public Class<IMoveEvent> getEventObjectType() {
            return IMoveEvent.class;
        }
    }

    protected class BaseDirectionChangeEventHandler implements EventHandler<IDirectionChangeEvent> {
        @Override
        public void handle(IDirectionChangeEvent event) {
            log.debug("BaseDirectionChangeEventHandler.handle with parameters " + "event = [" + event + "]");
            synchronized (MoveableUnit.this) {
                if (MoveableUnit.this.getDirection() != event.getNewDirection()) {
                    log.debug("BaseDirectionChangeEventHandler.handle Changing value from [" + MoveableUnit.this.getDirection() + "] to  " +
                            "[" + event.getNewDirection() + "]");
                    MoveableUnit.this.setDirection(event.getNewDirection());
                }
            }
        }

        @Override
        public Class<IDirectionChangeEvent> getEventObjectType() {
            return IDirectionChangeEvent.class;
        }
    }


    protected class EngineStateChangeEventHandler implements EventHandler<IEngineStateChangeEvent> {

        @Override
        public void handle(IEngineStateChangeEvent event) {
            log.debug("EngineStateChangeEventHandler.handle with parameters " + "event = [" + event + "]");
            synchronized (MoveableUnit.this) {
                if (MoveableUnit.this.getEngineState() != event.getNewEngineState()) {
                    log.debug("EngineStateChangeEventHandler.handle Changing value from [" + MoveableUnit.this.getEngineState() +
                            "] to  " +
                            "[" + event.getNewEngineState() + "]");
                    MoveableUnit.this.setEngineState(event.getNewEngineState());
                }
            }
        }

        @Override
        public Class<IEngineStateChangeEvent> getEventObjectType() {
            return IEngineStateChangeEvent.class;
        }
    }

}
