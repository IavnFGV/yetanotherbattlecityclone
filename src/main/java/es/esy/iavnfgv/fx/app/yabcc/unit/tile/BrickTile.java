package es.esy.iavnfgv.fx.app.yabcc.unit.tile;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.Direction;
import es.esy.iavnfgv.fx.app.yabcc.playground.Playground;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static java.util.Arrays.asList;

/**
 * Created by GFH on 10.01.2016.
 */
public class BrickTile extends Tile {
    private List<Bounds> complexBounds;
    private Bounds[] tileParts;
    private ObjectProperty<TileState> tileState = new SimpleObjectProperty<>(TileState.STATE_1111);

    public BrickTile(Playground playground) {
        super(playground);
    }

    private void createComplexBoundHandler() {
        Bounds topLeft = getBoundsList().get(0);
        tileParts = new Bounds[]{
                new BoundingBox(topLeft.getMinX(),
                        topLeft.getMinY(), 8, 8),
                new BoundingBox(topLeft.getMinX() + 8,
                        topLeft.getMinY(), 8, 8),
                new BoundingBox(topLeft.getMinX() + 8,
                        topLeft.getMinY() + 8, 8, 8),
                new BoundingBox(topLeft.getMinX(),
                        topLeft.getMinY() + 8, 8, 8)};
        tileStateProperty().addListener((observable, oldValue, newValue) -> {
            rebuildBounds(newValue);
        });
        rebuildBounds(getTileState());
    }

    public ObjectProperty<TileState> tileStateProperty() {
        return tileState;
    }

    private void rebuildBounds(TileState tileState) {
        tileState.boundsRebuilder.accept(this);
    }

    public TileState getTileState() {
        return tileState.get();
    }

    public void setTileState(TileState tileState) {
        this.tileState.set(tileState);
    }

    public enum TileState { // TODO implement transitions???
        STATE_EMPTY,
        STATE_0001,
        STATE_0010,
        STATE_0011(tileUnit -> tileUnit.complexBounds = asList(
                tileUnit.tileParts[2], tileUnit.tileParts[3]),
                null, STATE_0010, null, STATE_0001),
        STATE_0100,
        STATE_0101,
        STATE_0110(tileUnit -> tileUnit.complexBounds =
                asList(tileUnit.tileParts[1], tileUnit.tileParts[2]),
                STATE_0100, null, STATE_0010, null),
        STATE_0111,
        STATE_1000,
        STATE_1001(tileUnit -> tileUnit.complexBounds =
                asList(tileUnit.tileParts[0], tileUnit.tileParts[3]),
                STATE_1000, null, STATE_0001, null),
        STATE_1010,
        STATE_1011,
        STATE_1100(tileUnit -> tileUnit.complexBounds =
                asList(tileUnit.tileParts[0], tileUnit.tileParts[1]),
                null, STATE_0100, null, STATE_1000),
        STATE_1101,
        STATE_1110,
        STATE_1111(tileUnit -> tileUnit.complexBounds = asList(
                tileUnit.tileParts[0], tileUnit.tileParts[1],
                tileUnit.tileParts[2], tileUnit.tileParts[3]),
                STATE_1100, STATE_0110, STATE_0011, STATE_1001);

        protected Map<Direction, TileState> transitionMap = new HashMap<>();
        protected Consumer<BrickTile> boundsRebuilder;

        TileState(Consumer<BrickTile> boundsRebuilder, TileState... state) {
            this(state);
            this.boundsRebuilder = boundsRebuilder;
        }

        /**
         * Convention (UP,RIGHT,DOWN,LEFT) as a result {@code MoveableUnit.Direction.values()}.
         * If something absent or {@code null}, {@code getTileStateAfterBullet(BulletUnit bulletUnit)}
         * will return  {@code  STATE_EMPTY}
         */
        TileState(TileState... state) {
            Direction[] directions = Direction.values();
            for (int i = 0; i < state.length; i++) {
                if (state[i] == null) {
                    continue;
                }
                transitionMap.put(directions[i], state[i]);
            }
        }

//        public TileState getTileStateAfterBullet(BulletUnit bulletUnit) {
//            return transitionMap.getOrDefault(bulletUnit.getDirection(), STATE_EMPTY);
//        }
    }

}
