package es.esy.iavnfgv.fx.app.yabcc.unit.moveable;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.MoveOnWorldBounds;
import es.esy.iavnfgv.fx.app.yabcc.enumeration.VitalState;
import es.esy.iavnfgv.fx.app.yabcc.event.EventHandler;
import es.esy.iavnfgv.fx.app.yabcc.event.IBoundsChangeEvent;
import es.esy.iavnfgv.fx.app.yabcc.event.IDirectionChangeEvent;
import es.esy.iavnfgv.fx.app.yabcc.event.IMoveEvent;
import es.esy.iavnfgv.fx.app.yabcc.playground.Playground;
import es.esy.iavnfgv.fx.app.yabcc.unit.Unit;
import es.esy.iavnfgv.fx.app.yabcc.unit.tile.BrickTile;
import es.esy.iavnfgv.fx.app.yabcc.unit.tile.SteelTile;
import es.esy.iavnfgv.fx.app.yabcc.unit.tile.Tile;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

/**
 * Created by GFH on 11.01.2016.
 */
public class Tank extends MoveableUnit {
    public Tank(Playground playground) {
        super(playground);
    }

    @Override
    protected EventHandler<IDirectionChangeEvent> createDirectionChangeEventHandler() {
        return new TankDirectionChangeEventHandler();
    }

    @Override
    protected EventHandler<IMoveEvent> createMoveEventHandler() {
        return new TankMoveEventHandler();
    }

    @Override
    protected MoveOnWorldBounds isMoveAllowed(boolean isInWorldBounds) {
        return MoveOnWorldBounds.STOP;
    }

    protected class TankMoveEventHandler extends BaseMoveEventHandler {
        @Override
        protected void allowChange(List<Bounds> newBounds) {
            if (newBounds.size() > 1) {
                throw new RuntimeException("Multiple bounds for tanks are not implemented yet");
            }
            Bounds newSingleBounds = newBounds.get(0);
            Bounds curBounds = moveable.getBoundsList().get(0);
            // newBounds will always intersects another bounds if we are absolutely near another
            // so we give 1 pixel on every direction to handle this situation
            Bounds newFixedBounds = new BoundingBox(newSingleBounds.getMinX() + 1,
                    newSingleBounds.getMinY() + 1,
                    newSingleBounds.getWidth() - 2,
                    newSingleBounds.getHeight() - 2);
            Point2D result = new Point2D(newSingleBounds.getMinX(), newSingleBounds.getMinY());
            Predicate<Unit> notMe = u -> (u != moveable);
            Predicate<Unit> bricksAndSteelOrTanks = u -> {
                if (u instanceof Tile) {
                    Tile tile = (Tile) u;
                    if (tile instanceof BrickTile) return true;
                    if (tile instanceof SteelTile) return true;
                }
                return u instanceof Tank;
            };
            // Predicate<GameUnitX> notBullet = u -> (!(u instanceof BulletUnit));
            // Predicate<GameUnitX> notBonus = u -> (!(u instanceof BonusUnit));
            Predicate<Unit> notDead = u -> u.getVitalState() != VitalState.SLEEP;

            Point2D newPosition = playground.getUnitList().stream()
                    .filter(notMe)
                    .filter(bricksAndSteelOrTanks)
                    .filter(notDead)
                    .map(u -> {
                                if (u.getBounds().intersects(curBounds)) {
                                    switch (moveable.getDirection()) {
                                        case UP: {
                                            double deltaY = u.getBounds().getMaxY() - newSingleBounds.getMinY();
                                            return result.add(0, deltaY);
                                        }
                                        case LEFT: {
                                            double deltaX = u.getBounds().getMaxX() - newSingleBounds.getMinX();
                                            return result.add(deltaX, 0);
                                        }
                                        case DOWN: {
                                            double deltaY = newSingleBounds.getMaxY() - u.getBounds().getMinY();
                                            return result.add(0, -deltaY);

                                        }
                                        case RIGHT: {
                                            double deltaX = newSingleBounds.getMaxX() - u.getBounds().getMinX();
                                            return result.add(-deltaX, 0);
                                        }
                                    }
                                }
                                return result;
                            }
                    ).filter(point2D -> (!point2D.equals(result))).findFirst().orElse(result);
            super.allowChange(asList(new BoundingBox(newPosition.getX(),
                    newPosition.getY(),
                    curBounds.getWidth(),
                    curBounds.getHeight())));
        }
    }

    protected class TankDirectionChangeEventHandler extends BaseDirectionChangeEventHandler {

        @Override
        public void handle(IDirectionChangeEvent event) {
            super.handle(event);
            fixPosition();
        }

        public void fixPosition() {
            if (getBoundsList() == null) return;
            if (getBoundsList().size() > 1) {
                throw new RuntimeException("Multiple bounds fixing does not implemented yet");
            }
            Bounds curBounds = getBounds();
            long cellSize = Math.round(curBounds.getWidth() / 2);

            double newX = curBounds.getMinX();
            double newY = curBounds.getMinY();

            Long x = nearest(newX, cellSize);
            Long y = nearest(newY, cellSize);

            if (Math.abs(newX - x) < (cellSize / 2 + 1)) {
                newX = Double.valueOf(x);
            }
            if (Math.abs(newY - y) < (cellSize / 2 + 1)) {
                newY = Double.valueOf(y);
            }
            fixPosition(curBounds, newX, newY);
        }

        private long nearest(double num, long base) {
            return (Math.round(num / (base * 1.)) * base);
        }

        private void fixPosition(Bounds curBounds, Double newValueX, Double newValueY) {
            MoveableUnit moveable = Tank.this;
            Predicate<Unit> onlyTank = gameUnit -> (gameUnit instanceof Tank);
            Predicate<Unit> notMe = gameUnit -> (gameUnit != moveable);
            Predicate<Unit> onlyTankAndNotMe = onlyTank.and(notMe);
            Predicate<Unit> notDead = gameUnit1 -> gameUnit1.getVitalState() == VitalState.ACTIVE;
            List<Unit> tanks = playground.getUnitList().stream()
                    .filter(notDead)
                    .filter(onlyTankAndNotMe)
                    .collect(Collectors.toList());
            if (tanks.size() == 0) {// no more tanks
                moveable.handle((IBoundsChangeEvent) () -> asList(new BoundingBox(
                        newValueX, newValueY,
                        curBounds.getWidth(),
                        curBounds.getHeight())));
                return;
            }
            if (!tanks.stream().map(gameUnit -> {
                Bounds newBounds = new BoundingBox(newValueX + 1,
                        newValueY + 1,
                        curBounds.getWidth() - 2,
                        curBounds.getHeight() - 2);
                return newBounds.intersects(curBounds);
            }).anyMatch(b -> (b == true))) {
                moveable.handle((IBoundsChangeEvent) () -> asList(new BoundingBox(
                        newValueX, newValueY,
                        curBounds.getWidth(),
                        curBounds.getHeight())));
            }
        }

        @Override
        public Class<IDirectionChangeEvent> getEventObjectType() {
            return IDirectionChangeEvent.class;
        }
    }
}
