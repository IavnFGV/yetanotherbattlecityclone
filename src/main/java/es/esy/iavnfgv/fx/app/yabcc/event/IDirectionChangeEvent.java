package es.esy.iavnfgv.fx.app.yabcc.event;


import es.esy.iavnfgv.fx.app.yabcc.enumeration.Direction;

/**
 * Created by GFH on 21.11.2015.
 */
public interface IDirectionChangeEvent extends IChangeEvent {

    Direction getNewDirection();
}
