package es.esy.iavnfgv.fx.app.yabcc.event;

import es.esy.iavnfgv.fx.app.yabcc.unit.Unit;

import java.util.EventListener;

/**
 * Created by GFH on 10.01.2016.
 */
public interface EventHandler<E extends IChangeEvent> extends EventListener {
    /**
     * Invoked when a specific event of the type for which this handler is
     * registered happens.
     *
     * @param event the event which occurred
     */
    void handle(E event);

    Class<E> getEventObjectType();

    default <U extends Unit> boolean isAvailable(E event, U unit) {
        return true;
    }
}
