package es.esy.iavnfgv.fx.app.yabcc.event;

/**
 * Created by GFH on 21.11.2015.
 */
public interface IMoveEvent extends IChangeEvent {
    long getDeltaTime();
}
