package es.esy.iavnfgv.fx.app.yabcc.event;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.EngineState;

/**
 * Created by GFH on 22.11.2015.
 */
public interface IEngineStateChangeEvent extends IChangeEvent {
    EngineState getNewEngineState();
}
