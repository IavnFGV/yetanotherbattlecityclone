package es.esy.iavnfgv.fx.app.yabcc.playground;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.PlaygroundState;
import javafx.beans.property.ReadOnlyObjectProperty;

/**
 * Created by GFH on 11.01.2016.
 */
public interface Playground extends HasUnits, Loadable {

    ReadOnlyObjectProperty<PlaygroundState> stateProperty();

    double getTileWidth();

    double getTileHeight();
}
