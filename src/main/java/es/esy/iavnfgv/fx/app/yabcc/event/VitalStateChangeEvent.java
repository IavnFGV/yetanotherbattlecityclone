package es.esy.iavnfgv.fx.app.yabcc.event;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.VitalState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by GFH on 20.11.2015.
 */
public class VitalStateChangeEvent extends ChangeEvent implements IVitalStateChangeEvent {
    private static final Logger log = LoggerFactory.getLogger(VitalStateChangeEvent.class);
    private final VitalState newVitalState;

    public VitalStateChangeEvent(Object source, VitalState newVitalState) {
        super(source);
        this.newVitalState = newVitalState;
        log.debug("VitalStateChangeEvent.VitalStateChangeEvent with parameters " + "source = [" + source + "], newVitalState = [" + newVitalState + "]");
    }

    public VitalState getNewVitalState() {
        return newVitalState;
    }

    @Override
    public String toString() {
        return "VitalStateChangeEvent{" +
                "newVitalState=" + newVitalState +
                "} " + super.toString();
    }
}
