package es.esy.iavnfgv.fx.app.yabcc.enumeration;

/**
 * Created by GFH on 13.01.2016.
 */
public enum AnimationLayer {
    WATER,
    TANK,
    BULLET,
    TILE,
    GRASS,
    BONUS
}
