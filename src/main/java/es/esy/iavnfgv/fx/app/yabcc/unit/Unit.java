package es.esy.iavnfgv.fx.app.yabcc.unit;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.PauseState;
import es.esy.iavnfgv.fx.app.yabcc.enumeration.VitalState;
import es.esy.iavnfgv.fx.app.yabcc.event.*;
import es.esy.iavnfgv.fx.app.yabcc.playground.Playground;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;

/**
 * Be careful - ONE eventHandler for ONE property and ONE InterfaceEvent
 */
public class Unit implements EventHandler {

    private static Logger log = LoggerFactory.getLogger(Unit.class);
    protected List<EventHandler> eventHandlers = new ArrayList<>();//new LinkedList<>();
    protected ListProperty<Bounds> boundsList = new SimpleListProperty<>(FXCollections.observableArrayList(asList
            (new BoundingBox(0, 0, 0, 0))));
    protected ObjectProperty<VitalState> vitalState = new SimpleObjectProperty<>(VitalState.SLEEP);
    protected ObjectProperty<PauseState> pauseState = new SimpleObjectProperty<>(PauseState.PAUSE);

    protected Playground playground;

    public Unit(Playground playground) {
        eventHandlers.add(createVitalStateChangeEventHandler());
        eventHandlers.add(createBoundsChangeEventHandler());
        eventHandlers.add(createPauseStateChangeEventHandler());
        vitalStateProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == VitalState.SLEEP) {
                setPauseState(PauseState.PAUSE);
            }
        });
        this.playground = playground;
    }


    protected EventHandler<IVitalStateChangeEvent> createVitalStateChangeEventHandler() {
        return new VitalStateChangeEventHandler();
    }

    protected EventHandler<IBoundsChangeEvent> createBoundsChangeEventHandler() {
        return new BoundsChangeEventHandler();
    }

    protected EventHandler<IPauseStateChangeEvent> createPauseStateChangeEventHandler() {
        return new PauseChangeEventHandler();
    }

    public ObjectProperty<VitalState> vitalStateProperty() {
        return vitalState;
    }

    public static void main(String[] args) {
        Unit unit = new Unit(null);

        unit.handle(new WakeUpChangeEvent(unit, PauseState.PAUSE, VitalState.ACTIVE, null));
    }

    @Override
    @SuppressWarnings("unchecked")
    public void handle(IChangeEvent event) {
        eventHandlers.stream()
                .filter(eventHandler1 -> eventHandler1.getEventObjectType().isInstance(event))
                .filter(eventHandler2 -> eventHandler2.isAvailable(event, this))
                .forEach(eventHandler -> eventHandler.handle(event));
    }

    public Playground getPlayground() {
        return playground;
    }

    public void setPlayground(Playground playground) {
        this.playground = playground;
    }

    public PauseState getPauseState() {
        return pauseState.get();
    }

    public void setPauseState(PauseState pauseState) {
        this.pauseState.set(pauseState);
    }

    /**
     * return topleft bound from boundslist
     *
     * @return Bounds
     */
    public Bounds getBounds() {
        return getBoundsList().get(0);
    }

    public ObservableList<Bounds> getBoundsList() {
        return boundsList.get();
    }

    public void setBoundsList(ObservableList<Bounds> boundsList) {
        this.boundsList.set(boundsList);
    }

    public ObjectProperty<PauseState> pauseStateProperty() {
        return pauseState;
    }

    public VitalState getVitalState() {
        return vitalState.get();
    }

    public void setVitalState(VitalState vitalState) {
        this.vitalState.set(vitalState);
    }

    public ListProperty<Bounds> boundsListProperty() {
        return boundsList;
    }

    public Class getEventObjectType() {
        return null;
    }

    protected class VitalStateChangeEventHandler implements EventHandler<IVitalStateChangeEvent> {
        @Override
        public void handle(IVitalStateChangeEvent event) {
            Unit.this.setVitalState(event.getNewVitalState());
        }

        @Override
        public Class<IVitalStateChangeEvent> getEventObjectType() {
            return IVitalStateChangeEvent.class;
        }

        @Override
        public boolean isAvailable(IVitalStateChangeEvent event, Unit unit) {
            return true;
        }
    }

    protected class BoundsChangeEventHandler implements EventHandler<IBoundsChangeEvent> {
        @Override
        public void handle(IBoundsChangeEvent event) {
            log.debug("BoundsChangeEventHandler.handle with parameters " + "event = [" + event + "]");
            synchronized (Unit.this) {
                if (Unit.this.getBoundsList().containsAll(event.getNewBounds()) &&
                        (Unit.this.getBoundsList().size() == event.getNewBounds().size())) {
                    log.debug("BoundsChangeEventHandler.handle lists are equal");
                    return;
                }
                log.debug("BoundsChangeEventHandler.handle replacing bounds");
                Unit.this.getBoundsList().clear();
                Unit.this.getBoundsList().addAll(event.getNewBounds());
            }
        }

        @Override
        public Class<IBoundsChangeEvent> getEventObjectType() {
            return IBoundsChangeEvent.class;
        }
    }

    protected class PauseChangeEventHandler implements EventHandler<IPauseStateChangeEvent> {
        @Override
        public void handle(IPauseStateChangeEvent event) {
            log.debug("PauseChangeEventHandler.handle with parameters " + "event = [" + event + "]");
            synchronized (Unit.this) {
                if (Unit.this.getPauseState() != event.getNewPauseState()) {
                    log.debug("PauseChangeEventHandler.handle Changing value from [" + Unit.this.getPauseState() + "] to  " +
                            "[" + event.getNewPauseState() + "]");
                    Unit.this.setPauseState(event.getNewPauseState());
                }
            }
        }

        @Override
        public Class<IPauseStateChangeEvent> getEventObjectType() {
            return IPauseStateChangeEvent.class;
        }
    }


}
