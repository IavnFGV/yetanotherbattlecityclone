package es.esy.iavnfgv.fx.app.yabcc.event;


import es.esy.iavnfgv.fx.app.yabcc.enumeration.PauseState;

/**
 * Created by GFH on 21.11.2015.
 */
public interface IPauseStateChangeEvent extends IChangeEvent {
    PauseState getNewPauseState();
}
