package es.esy.iavnfgv.fx.app.yabcc.enumeration;

/**
 * Created by GFH on 11.01.2016.
 */
public enum MoveOnWorldBounds {
    ALLOW,
    STOP,
    DESTROY
}
