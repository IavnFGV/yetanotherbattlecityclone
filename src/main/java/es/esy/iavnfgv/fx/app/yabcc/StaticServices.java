package es.esy.iavnfgv.fx.app.yabcc;

import java.util.Locale;

/**
 * Created by GFH on 06.07.2015.
 */
public class StaticServices {
    public static Locale DEFAULT_LOCALE = Locale.getDefault();
    //    public static ResourceBundle MESSAGES = ResourceBundle.getBundle("messages/messages", DEFAULT_LOCALE);
    public static Long ONE_SECOND = 1000_000_000l;
    public static long FAST_SPEED = 8l;
    public static long NORMAL_SPEED = 6l;
    public static long BULLET_CASH_SIZE = 6l;
    public static long NORMAL_BULLET_SPEED = 10l;
    public static long FAST_BULLET_SPEED = 14l;
    public static Long BULLET_EXPLODING_TIME = ONE_SECOND / 24;
    public static Long TANK_EXPLODING_TIME = ONE_SECOND / 16;

    public static int EXTRACT_COLLIDING_LIST = 1;
    public static int WATER_ANIMATION_DURATION = 600;//ms

    public static int CELL_HEIGHT=8;
    public static int CELL_WIDTH=8;
    public static int WORLD_WIDTH_CELLS = 26;
    public static int WORLD_HEIGHT_CELLS= 26;
    public static int TANK_HEIGHT_CELLS= 2;
    public static int TANK_WIDTH_CELLS = 2;
    public static int BONUS_HEIGHT_CELLS = 2;
    public static int BONUS_WIDTH_CELLS = 2;
    public static int EAGLE_HEIGHT_CELLS = 2;
    public static int EAGLE_WIDTH_CELLS = 2;
    public static double BULLET_HEIGHT_CELLS= 1 / 2.;
    public static double BULLET_WIDTH_CELLS = 1 / 2.;
    //private int worldSizeCells = worldWidthCells * worldHeightCells;


}
