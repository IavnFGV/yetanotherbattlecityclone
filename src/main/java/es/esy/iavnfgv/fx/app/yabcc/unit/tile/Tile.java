package es.esy.iavnfgv.fx.app.yabcc.unit.tile;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.TileBasicState;
import es.esy.iavnfgv.fx.app.yabcc.playground.Playground;
import es.esy.iavnfgv.fx.app.yabcc.unit.Unit;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * Created by GFH on 10.01.2016.
 */
public class Tile extends Unit {
    private ObjectProperty<TileBasicState> basicState = new SimpleObjectProperty<>();

    public Tile(Playground playground) {
        super(playground);
    }

    public TileBasicState getBasicState() {
        return basicState.get();
    }

    public void setBasicState(TileBasicState basicState) {
        this.basicState.set(basicState);
    }

    public ObjectProperty<TileBasicState> basicStateProperty() {
        return basicState;
    }
}
