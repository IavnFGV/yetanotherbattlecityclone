package es.esy.iavnfgv.fx.app.yabcc.event;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.VitalState;

/**
 * Created by GFH on 21.11.2015.
 */
public interface IVitalStateChangeEvent extends IChangeEvent {
    VitalState getNewVitalState();
}
