package es.esy.iavnfgv.fx.app.yabcc.playground;

import es.esy.iavnfgv.fx.app.yabcc.unit.Unit;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;

import java.util.List;

/**
 * Created by GFH on 11.01.2016.
 */
public interface HasUnits {
    ObservableList<Unit> getUnitList();

    boolean isInWorldBounds(List<Bounds> boundsList);
}
