package es.esy.iavnfgv.fx.app.yabcc.unit.tile;

import es.esy.iavnfgv.fx.app.yabcc.playground.Playground;

/**
 * Created by GFH on 10.01.2016.
 */
public class SteelTile extends Tile {
    public SteelTile(Playground playground) {
        super(playground);
    }
}
