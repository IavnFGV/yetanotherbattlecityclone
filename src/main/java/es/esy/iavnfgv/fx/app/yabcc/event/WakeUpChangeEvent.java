package es.esy.iavnfgv.fx.app.yabcc.event;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.PauseState;
import es.esy.iavnfgv.fx.app.yabcc.enumeration.VitalState;
import javafx.geometry.Bounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by GFH on 21.11.2015.
 */
public class WakeUpChangeEvent extends ChangeEvent implements IVitalStateChangeEvent, IPauseStateChangeEvent, IBoundsChangeEvent {
    private static final Logger log = LoggerFactory.getLogger(WakeUpChangeEvent.class);

    private final PauseState newPauseState;
    private final VitalState newBasicState;
    private final List<Bounds> newBounds;

    public WakeUpChangeEvent(Object source, PauseState newPauseState, VitalState newBasicState, List<Bounds> newBounds) {
        super(source);
        log.debug("WakeUpChangeEvent.WakeUpChangeEvent with parameters " + "source = [" + source + "], newPauseState = ["
                + newPauseState + "], newBasicState = [" + newBasicState + "], newBounds = [" + newBounds + "]");
        this.newPauseState = newPauseState;
        this.newBasicState = newBasicState;
        this.newBounds = newBounds;
    }

    @Override
    public List<Bounds> getNewBounds() {
        return newBounds;
    }

    public VitalState getNewVitalState() {
        return newBasicState;
    }

    @Override
    public PauseState getNewPauseState() {
        return newPauseState;
    }
}
