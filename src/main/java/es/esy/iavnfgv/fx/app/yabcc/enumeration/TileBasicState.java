package es.esy.iavnfgv.fx.app.yabcc.enumeration;

/**
 * Created by GFH on 10.01.2016.
 */
public enum TileBasicState {
    ACTIVE,
    DEAD
}
