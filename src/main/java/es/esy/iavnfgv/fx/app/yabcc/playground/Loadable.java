package es.esy.iavnfgv.fx.app.yabcc.playground;

import es.esy.iavnfgv.fx.app.yabcc.unit.Unit;

/**
 * Created by GFH on 11.01.2016.
 */
public interface Loadable {
    boolean load(int x, int y, Unit unit);

}
