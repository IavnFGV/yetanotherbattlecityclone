package es.esy.iavnfgv.fx.sprite;

import es.esy.iavnfgv.fx.app.yabcc.StaticServices;
import es.esy.iavnfgv.fx.app.yabcc.enumeration.AnimationLayer;
import es.esy.iavnfgv.fx.app.yabcc.unit.tile.WaterTile;
import javafx.scene.image.Image;
import javafx.util.Duration;

/**
 * Created by GFH on 13.01.2016.
 */
public class WaterFxSprite extends FxSprite<WaterTile> {
    public WaterFxSprite(WaterTile unit) {
        super(unit);
        backAnimations.put(YabccSprite.TILE_WATER, new WaterAnimation(
                Duration.millis(StaticServices.WATER_ANIMATION_DURATION)
                , baseImage
                , YabccSprite.TILE_WATER
        ));
    }

    protected class WaterAnimation extends SpriteAnimation {
        private int count;
        private int lastIndex;
        public WaterAnimation(Duration duration, Image image, YabccSprite yabccSprite) {
            super(duration, image, yabccSprite, AnimationLayer.WATER);
            setCycleCount(INDEFINITE);
            count = viewports.length;
        }

        @Override
        protected void interpolate(double k) {
            if (!imageView.isVisible()) return;
            final int index = Math.min((int) Math.floor(k * count), count - 1);
            if (index != lastIndex) {
                imageView.setViewport(viewports[index]);
                lastIndex = index;
            }
        }
    }
}
