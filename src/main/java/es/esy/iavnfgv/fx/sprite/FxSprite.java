package es.esy.iavnfgv.fx.sprite;

import es.esy.iavnfgv.fx.app.yabcc.enumeration.AnimationLayer;
import es.esy.iavnfgv.fx.app.yabcc.enumeration.VitalState;
import es.esy.iavnfgv.fx.app.yabcc.unit.Unit;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.beans.InvalidationListener;
import javafx.beans.binding.BooleanBinding;
import javafx.geometry.Bounds;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by GFH on 13.01.2016.
 */
public /*abstract*/ class FxSprite<U extends Unit> extends Group {
    protected static Image baseImage = YabccSprite.baseImage;
    private static Logger log = LoggerFactory.getLogger(FxSprite.class);
    protected Map<YabccSprite, SpriteAnimation> animations = new LinkedHashMap<>();
    protected Map<YabccSprite, SpriteAnimation> backAnimations = new LinkedHashMap<>();

    protected U unit;

    protected BooleanBinding spriteVisibilityBinding;

    public FxSprite(U unit) {
        this.unit = unit;
        createVisibilityBinding();
        bindVisibility();
    }

    protected BooleanBinding createVisibilityBinding() {
        spriteVisibilityBinding = unit.vitalStateProperty().isEqualTo(VitalState.ACTIVE);
        return spriteVisibilityBinding;
    }

    protected void bindVisibility() {
        visibleProperty().bind(spriteVisibilityBinding);
    }

    protected abstract class PausableAnimation extends SpriteAnimation {
        {
//            unit.pauseStateProperty().addListener((observable, oldValue, newValue) -> {
//                if (newValue == PauseState.PLAY) {
//                    this.play();
//                }
//                if (newValue == PauseState.PAUSE) {
//                    this.pause();
//                }
//            });
        }

        public PausableAnimation(Image image, YabccSprite yabccSprite, AnimationLayer layer) {
            this(Duration.INDEFINITE, image, yabccSprite, layer);
        }

        public PausableAnimation(Duration duration, Image image, YabccSprite yabccSprite, AnimationLayer layer) {
            super(duration, image, yabccSprite, layer);
            bindPlayingState();
        }

        protected abstract void bindPlayingState();

        public PausableAnimation(Duration duration, Image image, YabccSprite yabccSprite, AnimationLayer layer, double dx, double dy) {
            super(duration, image, yabccSprite, layer, dx, dy);
            bindPlayingState();
        }

        public PausableAnimation(Image image, YabccSprite yabccSprite, AnimationLayer layer, double dx, double dy) {
            super(image, yabccSprite, layer, dx, dy);
            bindPlayingState();
        }
    }


    protected abstract class SpriteAnimation extends Transition {

        protected ImageView imageView = new ImageView();
        protected boolean inBackground = false;
        protected Rectangle2D[] viewports;
        protected AnimationLayer layer;

        protected BooleanBinding visibleBinding;

        public SpriteAnimation(
                Duration duration
                , Image image
                , YabccSprite yabccSprite
                , AnimationLayer layer
        ) {
            this(duration, image, yabccSprite, layer, 0, 0);
        }

        public SpriteAnimation(
                Duration duration
                , Image image
                , YabccSprite yabccSprite
                , AnimationLayer layer
                , double dx
                , double dy
        ) {
            this(image, yabccSprite, layer, dx, dy);
            setCycleDuration(duration);
            setInterpolator(Interpolator.LINEAR);
        }

        public SpriteAnimation(Image image
                , YabccSprite yabccSprite
                , AnimationLayer layer
                , double dx
                , double dy) {
            this.imageView.setImage(image);
            this.viewports = yabccSprite.viewports;
            this.imageView.setViewport(this.viewports[0]);
            this.layer = layer;
            bindImageViewToGameUnit(imageView, dx, dy);
            bindVisibility();
            FxSprite.this.getChildren().add(imageView);
        }

        /**
         * Of course its maybe not best decision, but we have to choose one..
         *
         * @param imageView
         * @param dx
         * @param dy
         */
        protected final void bindImageViewToGameUnit(ImageView imageView, double dx, double dy) {
            unit.getBoundsList().addListener((InvalidationListener) observable -> {
                List<Bounds> list = (List<Bounds>) observable;
                imageView.setX(list.get(0).getMinX() + dx);
                imageView.setY(list.get(0).getMinY() + dy);
            });
            imageView.setX(unit.getBoundsList().get(0).getMinX() + dx);
            imageView.setY(unit.getBoundsList().get(0).getMinY() + dy);

        }

        protected void bindVisibility() {

            visibleBinding = unit.vitalStateProperty().isEqualTo(VitalState.ACTIVE);
            imageView.visibleProperty().bind(visibleBinding);
        }


        public AnimationLayer getLayer() {
            return layer;
        }

        public ImageView getImageView() {
            return imageView;
        }

        protected boolean isInBackground() {
            return inBackground;
        }


    }

    protected class BlankSpriteAnimation extends SpriteAnimation {
        public BlankSpriteAnimation(Image image, YabccSprite yabccSprite, AnimationLayer layer) {
            super(image, yabccSprite, layer, 0, 0);
        }

        @Override
        protected void interpolate(double frac) {
            log.debug("BlankSpriteAnimation.interpolate");
        }
    }


}
